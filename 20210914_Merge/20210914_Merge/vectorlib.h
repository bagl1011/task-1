/**************** 20210914_MERGE *************************************
*
***************** VECTORLIB_H ****************************************
* 
* PURPOSE:	declare merge() function
*			declare isSorted() function	
* 
* CREATED: 14.09.2021 by Gleb Baklagin <bagl1011@h-ka.de>
* 
* LAST MODIFIED: 14.09.2021 by Gleb Baklagin:
* 
* TODO:
* 
* SEE ALSO: MAIN_CPP, MERGE_CPP
* 
* *********************************************************************/

#pragma once

/* Own header files */

/* Misc header files */

/* Own defines */

/*!
* \brief	This function merges two sorted
*			int vectors and stores the result
*			in a 3rd vector (big enought to store
*			v1 and v2's values)
*			
* \param	pointer to 1st vector
*			1st vector's length (int)
*			pointer to 2nd vector
*			2nd vector's length (int)
*			pointer to destination vector
*
* \return	destination vector's lenght if
*			v1 and v2 are sorted
* 
*			-1 if v1 or v2 not sorted
*
* This function does the following:
* 
*/
int merge(int* v1, int v1Len, int* v2, int v2Len, int* vDest);

/*!
*
* \brief	This function checks wheter an int vector
*			is sorted 
*
* \param	pointer to vector
*			vector's lenth
*
* \return	true if vec is sorted
*			false if vec is not sorted
*/
bool isSorted(int* vec, int vecLen);